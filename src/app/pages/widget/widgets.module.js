/**
 * @author v.lugovsky
 * created on 16.12.2015
 */
(function () {
  'use strict';

  angular.module('BlurAdmin.pages.widgets', [])
      .config(routeConfig);

  /** @ngInject */
  function routeConfig($stateProvider) {
    $stateProvider
        .state('widgets', {
          url: '/widgets',
          templateUrl: 'app/pages/widget/widgets.html',
          title: 'Виджеты',
          sidebarMeta: {
            icon: 'ion-android-archive',
            order: 0,
          },
        });
  }

})();
